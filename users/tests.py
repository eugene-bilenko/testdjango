from datetime import datetime

from django.contrib.auth import get_user_model
from django.test import TestCase

from users.templatetags.users_templatetag import allowed, bizzfuzz

User = get_user_model()


class UserTestCase(TestCase):
    def setUp(self):
        User.objects.create(
            username='test',
            email='test@test.dev',
            first_name='test',
            last_name='test',
            birthday=datetime.strptime('2000-04-15', '%Y-%m-%d'),
            is_active=True,
        )
        User.objects.create(
            username='test2',
            email='test2@test.dev',
            first_name='test2',
            last_name='test2',
            birthday=datetime.strptime('2004-06-05', '%Y-%m-%d'),
            is_active=True,
        )
        User.objects.create(
            username='test3',
            email='test3@test.dev',
            first_name='test3',
            last_name='test3',
            birthday=datetime.strptime('2000-06-05', '%Y-%m-%d'),
            is_active=True,
        )

    def test_allowed_age(self):
        user1 = User.objects.get(username='test')
        user2 = User.objects.get(username='test2')

        self.assertEqual(allowed(user1.birthday), 'allowed')
        self.assertEqual(allowed(user2.birthday), 'blocked')

    def test_bizzfuzz(self):
        user1 = User.objects.get(username='test')
        user2 = User.objects.get(username='test2')
        user3 = User.objects.get(username='test3')

        user1.random_number = 36
        user2.random_number = 30
        user3.random_number = 40

        self.assertEqual(bizzfuzz(user1.random_number), 'Bizz')
        self.assertEqual(bizzfuzz(user2.random_number), 'BizzFuzz')
        self.assertEqual(bizzfuzz(user3.random_number), 'Fuzz')
