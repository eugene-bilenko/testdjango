from random import random

from django.contrib.auth import get_user_model
from django.db.models.signals import pre_save
from django.dispatch import receiver

User = get_user_model()


@receiver(pre_save, sender=User)
def before_user_create(sender, instance, **kwargs):
    instance.random_number = round(random() * 100)
