from django.contrib.auth import get_user_model
from django.http import HttpResponse
from django.template import Context
from django.template import loader
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import UpdateView

User = get_user_model()


class UsersView(ListView):
    template_name = 'users/users_list.html'
    model = User


class UserDetailView(DetailView):
    template_name = 'users/users_detail.html'
    model = User


class UserCreateView(CreateView):
    template_name = 'users/users_create.html'
    model = User
    fields = ['username', 'email', 'first_name', 'last_name', 'birthday', 'is_active', 'is_staff', 'is_superuser']


class UserDeleteView(DeleteView):
    model = User
    success_url = reverse_lazy('users:users-list')


class UserUpdateView(UpdateView):
    template_name = 'users/users_edit.html'
    model = User
    fields = ['username', 'email', 'first_name', 'last_name', 'birthday', 'is_active', 'is_staff', 'is_superuser']
