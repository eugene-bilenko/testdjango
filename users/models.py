from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse


class User(AbstractUser):
    birthday = models.DateField(null=True)
    random_number = models.IntegerField(default=0)

    def get_absolute_url(self):
        return reverse('users:users-list')
