from django.conf.urls import url

from users import views

urlpatterns = [
    url(r'^$', views.UsersView.as_view(), name='users-list'),
    url(r'^user/create/$', views.UserCreateView.as_view(), name='users-create'),
    url(r'^user/(?P<pk>\d+)/delete/$', views.UserDeleteView.as_view(), name='users-delete'),
    url(r'^user/(?P<pk>\d+)/$', views.UserDetailView.as_view(), name='users-detail'),
    url(r'^user/(?P<pk>\d+)/edit/$', views.UserUpdateView.as_view(), name='users-edit'),
]
