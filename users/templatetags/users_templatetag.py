from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.utils import timezone
from django import template

register = template.Library()


@register.simple_tag
def allowed(birthday):
    status = 'blocked'
    age = relativedelta(timezone.now().date(), birthday).years
    if age > settings.ALLOWED_USER_AGE:
        status = 'allowed'
    return status


@register.simple_tag
def bizzfuzz(number):
    status = ''
    if number % 3 == 0:
        status += 'Bizz'
    if number % 5 == 0:
        status += 'Fuzz'
    if not status:
        status = str(number)

    return status

